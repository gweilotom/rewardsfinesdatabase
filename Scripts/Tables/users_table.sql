-- public.users definition

-- Drop table

-- DROP TABLE public.users;

CREATE TABLE public.users (
	username text NOT NULL,
	"name" text NOT NULL,
	surname text NOT NULL,
	"password" text NOT NULL,
	executive bool NOT NULL DEFAULT false,
	email text NOT NULL,
	"language" text NULL,
	CONSTRAINT "unique_USERS_USERID" UNIQUE (username),
	CONSTRAINT users_email_key UNIQUE (email),
	CONSTRAINT users_pkey PRIMARY KEY (name, surname)
);
CREATE INDEX users_name_idx ON public.users USING btree (name);
CREATE INDEX users_surname_idx ON public.users USING btree (surname);

-- Table Triggers

create trigger update_player_trigger after
insert
    or
update
    on
    public.users for each row execute function update_player();
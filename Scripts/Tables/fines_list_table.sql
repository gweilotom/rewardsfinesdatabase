-- public.fines_list definition

-- Drop table

-- DROP TABLE public.fines_list;

CREATE TABLE public.fines_list (
	id int4 NOT NULL DEFAULT nextval('fineidseq'::regclass),
	fineid int4 NOT NULL,
	playerid int4 NOT NULL,
	finedate date NULL,
	finepaid bool NOT NULL DEFAULT false
);
-- public.players definition

-- Drop table

-- DROP TABLE public.players;

CREATE TABLE public.players (
	"name" text NOT NULL,
	surname text NOT NULL,
	username text NULL,
	sex bpchar(1) NOT NULL,
	youth bool NOT NULL DEFAULT false,
	id int4 NOT NULL DEFAULT nextval('playeridseq'::regclass),
	dateofbirth date NOT NULL,
	CONSTRAINT players_pkey PRIMARY KEY (name, surname),
	CONSTRAINT unique_players_username UNIQUE (username)
);
CREATE INDEX index_name ON public.players USING btree (name);
CREATE INDEX index_surname ON public.players USING btree (surname);

-- Table Triggers

create trigger connect_user_trigger after
insert
    or
update
    on
    public.players for each row execute function connect_user();
-- public.fines_types definition

-- Drop table

-- DROP TABLE public.fines_types;

CREATE TABLE public.fines_types (
	id int4 NOT NULL DEFAULT nextval('finetypeseq'::regclass),
	description text NOT NULL,
	monetary_1st float8 NOT NULL,
	points_1st float8 NOT NULL,
	monetary_2nd float8 NULL,
	points_2nd float8 NULL,
	monetary_3rd float8 NULL,
	points_3rd float8 NULL,
	rising bool NOT NULL DEFAULT false,
	active bool NOT NULL DEFAULT true
);
-- public.rewards_list definition

-- Drop table

-- DROP TABLE public.rewards_list;

CREATE TABLE public.rewards_list (
	id int4 NOT NULL DEFAULT nextval('rewardidseq'::regclass),
	rewardid int4 NOT NULL,
	playerid int4 NOT NULL,
	rewarddate date NULL,
	multiplier float8 NOT NULL,
	bonus float8 NULL
);
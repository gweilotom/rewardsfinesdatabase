-- public.rewards_types definition

-- Drop table

-- DROP TABLE public.rewards_types;

CREATE TABLE public.rewards_types (
	id int4 NOT NULL DEFAULT nextval('rewardtypeseq'::regclass),
	description text NOT NULL,
	points float8 NOT NULL,
	active bool NOT NULL DEFAULT true
);
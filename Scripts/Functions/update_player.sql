CREATE OR REPLACE FUNCTION public.update_player()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
	BEGIN
		update players
			set username = new.username
			where "name" = new."name"
			and surname = new.surname;
		return new;
	END;
$function$
;

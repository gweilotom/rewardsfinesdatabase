CREATE OR REPLACE FUNCTION public.calc_unpaid_fines(pid integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
DECLARE
		unpaidfine numeric;
		fine integer;
		ufines integer[];

		rising boolean;
		monetary_1st numeric;
		monetary_2nd numeric;
		monetary_3rd numeric;

		total numeric;

		states boolean[];
		state boolean;
		counter integer;
	BEGIN
		unpaidfine := 0;
		ufines := array(
			select distinct fl.fineid
			from public.fines_list fl
				where fl.playerid = pid
				and not fl.finepaid);

		foreach fine in array ufines loop
			select ft.rising, ft.monetary_1st , ft.monetary_2nd , ft.monetary_3rd 
				into rising, monetary_1st, monetary_2nd, monetary_3rd
			from public.fines_types ft
				where ft.id = fine;

			select count(*)
				into total
			from public.fines_list fl2
				where fl2.playerid = pid
				and fl2.fineid = fine;

			case when not rising then
				unpaidfine := unpaidfine + (total * monetary_1st);
			else
				case when total = 1 then
					unpaidfine := unpaidfine + monetary_1st;
				when total = 2 then
					unpaidfine := unpaidfine + monetary_1st + monetary_2nd;
				when total >= 3 then
					unpaidfine := unpaidfine + monetary_1st + monetary_2nd + (monetary_3rd * (total - 2));
				else
					unpaidfine := unpaidfine;
				end case;
			end case;
		end loop;
		return unpaidfine;
	END;
$function$
;

CREATE OR REPLACE FUNCTION public.connect_user()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
	BEGIN
		select us.username
			into new.username
		from users us
			where us."name" = new."name"
			and us.surname = new.surname;
		return new;
	END;
$function$
;

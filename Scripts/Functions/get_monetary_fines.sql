CREATE OR REPLACE FUNCTION public.get_monetary_fines(pid integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
	DECLARE
		fmonetary numeric;
	BEGIN
		select fov.unpaid
			into fmonetary
		from public.fines_ovv_vd fov
			where fov.playerid = pid;
		return fmonetary;
	END;
$function$
;

CREATE OR REPLACE FUNCTION public.get_points_rewards(pid integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
	DECLARE
		rpoints numeric;
	BEGIN
		select rov.points
			into rpoints
		from public.rewards_ovv_vd rov
			where rov.playerid = pid;
		return rpoints;
	END;
$function$
;

CREATE OR REPLACE FUNCTION public.get_points_fines(pid integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
	DECLARE
		fpoints numeric;
	BEGIN
		select fov.points
			into fpoints
		from public.fines_ovv_vd fov
			where fov.playerid = pid;
		return fpoints;
	END;
$function$
;

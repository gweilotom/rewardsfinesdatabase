CREATE OR REPLACE FUNCTION public.calc_points_fines(pid integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
DECLARE
		pointsfine numeric;
		fine integer;
		ufines integer[];

		rising boolean;
		points_1st numeric;
		points_2nd numeric;
		points_3rd numeric;

		total numeric;

		states boolean[];
		state boolean;
		counter integer;
	BEGIN
		pointsfine := 0;
		ufines := array(
			select distinct fl.fineid
			from public.fines_list fl
				where fl.playerid = pid);

		foreach fine in array ufines loop
			select ft.rising, ft.points_1st , ft.points_2nd , ft.points_3rd 
				into rising, points_1st, points_2nd, points_3rd
			from public.fines_types ft
				where ft.id = fine;

			select count(*)
				into total
			from public.fines_list fl2
				where fl2.playerid = pid
				and fl2.fineid = fine;

			case when not rising then
				pointsfine := pointsfine + (total * points_1st);
			else
				case when total = 1 then
					pointsfine := pointsfine + points_1st;
				when total = 2 then
					pointsfine := pointsfine + points_1st + points_2nd;
				when total >= 3 then
					pointsfine := pointsfine + points_1st + points_2nd + (points_3rd * (total - 2));
				else
					pointsfine := pointsfine;
				end case;
			end case;
		end loop;
		return pointsfine;
	END;
$function$
;

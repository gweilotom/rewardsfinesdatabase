CREATE OR REPLACE FUNCTION public.calc_points_rewards(pid integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
DECLARE
		pointsreward numeric;
		reward integer;
		urewards integer[];

		points numeric;

		multipliers numeric[];
		multiplier numeric;
		boni numeric[];
		bonus numeric;

		states boolean[];
		state boolean;
		counter integer;
	BEGIN
		pointsreward := 0;
		urewards := array(
			select distinct rl.rewardid
			from public.rewards_list rl
				where rl.playerid = pid);

		foreach reward in array urewards loop
			select rt.points 
				into points
			from public.rewards_types rt
				where rt.id = reward;

			multipliers := array(
				select rl2.multiplier
				from public.rewards_list rl2
					where rl2.rewardid = reward
					and rl2.playerid = pid
					and rl2.multiplier is not null);

			boni := array(
				select rl3.bonus
				from public.rewards_list rl3
					where rl3.rewardid = reward
					and rl3.playerid = pid
					and rl3.bonus is not null);

			foreach multiplier in array multipliers loop
				pointsreward := pointsreward + (points * multiplier);
			end loop;

			foreach bonus in array boni loop
				pointsreward := pointsreward + bonus;
			end loop;
		end loop;
		return pointsreward;
	END;
$function$
;

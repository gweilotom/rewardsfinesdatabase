CREATE OR REPLACE FUNCTION public.calc_total_fines(pid integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
	DECLARE
		finetotal numeric;

		rpoints numeric;
		ptotal numeric;
	BEGIN
		select fov.monetary, fov.points
			into finetotal, ptotal
		from public.fines_ovv_vd fov
			where fov.playerid = pid;

		select rov.points
			into rpoints
		from public.rewards_ovv_vd rov
			where rov.playerid = pid;

		ptotal := ptotal + rpoints;

		case when ptotal < 0 then
			return finetotal + (ptotal * -2);
		else
			return finetotal;
		end case;
	END;
$function$
;

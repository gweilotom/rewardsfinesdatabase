-- public.rewards_id_seq definition

-- DROP SEQUENCE public.rewards_id_seq;

CREATE SEQUENCE public.rewards_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;
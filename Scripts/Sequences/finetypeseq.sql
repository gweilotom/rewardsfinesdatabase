-- public.finetypeseq definition

-- DROP SEQUENCE public.finetypeseq;

CREATE SEQUENCE public.finetypeseq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
-- public.rewards_ovv_vd source

CREATE OR REPLACE VIEW public.rewards_ovv_vd
AS SELECT p.id AS playerid,
    p.name,
    p.surname,
    p.username,
    p.sex,
    p.youth,
    calc_points_rewards(p.id) AS points
   FROM players p
  ORDER BY (calc_points_rewards(p.id));
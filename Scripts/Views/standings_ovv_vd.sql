-- public.standings_ovv_vd source

CREATE OR REPLACE VIEW public.standings_ovv_vd
AS SELECT p.id AS playerid,
    p.name,
    p.surname,
    p.username,
    p.sex,
    p.youth,
    get_monetary_fines(p.id) AS finesopen,
    get_points_fines(p.id) AS finepoints,
    get_points_rewards(p.id) AS rewardpoints,
    calc_total_fines(p.id) AS finestotal,
    get_points_rewards(p.id) - get_points_fines(p.id) AS pointstotal
   FROM players p
  ORDER BY (get_points_rewards(p.id) - get_points_fines(p.id));
-- public.fines_ovv_vd source

CREATE OR REPLACE VIEW public.fines_ovv_vd
AS SELECT p.id AS playerid,
    p.name,
    p.surname,
    p.username,
    p.sex,
    p.youth,
    calc_unpaid_fines(p.id) AS unpaid,
    calc_monetary_fines(p.id) AS monetary,
    calc_points_fines(p.id) AS points
   FROM players p
  ORDER BY (calc_points_fines(p.id)) DESC;